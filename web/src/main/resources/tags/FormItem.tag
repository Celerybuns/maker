<%@ tag language="java" pageEncoding="UTF-8"  body-content="scriptless"%>
<%@ attribute name="label" required="true"%>
<%@ attribute name="name" required="true"%>

<div class="form-group row">
    <label for="${name}" class="col-sm-2 col-form-label">${label}</label>
    <div class="col-sm-10">
        <jsp:doBody/>
    </div>
</div>