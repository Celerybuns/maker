security:
  oauth2:
    client:
      client-id: sso-demo
      client-secret: sso-demo-1234
      user-authorization-uri: http://118.190.60.69/oauth/authorize
      access-token-uri: http://118.190.60.69/oauth/token
    resource:
      jwt:
        key-uri: http://118.190.60.69/oauth/token_key
      user-info-uri: http://118.190.60.69/userInfo

spring:
  profiles:
    active: dev

  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true&zeroDateTimeBehavior=CONVERT_TO_NULL
    username: ${dbConnectionData.userNameProperty}
    password: ${dbConnectionData.passwordProperty}
    hikari:
      minimum-idle: 5
      maximum-pool-size: 15
      auto-commit: true
      idle-timeout: 30000
      pool-name: DatebookHikariCP
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1

  cache:
    ehcache:
      config: ehcache.xml

server:
  port: 8089

management:
  endpoint:
    health:
      show-details: always
    shutdown:
      enabled: false
    env:
      enabled: false
  endpoints:
    web:
      exposure:
        include: health,info,metrics,threaddump,heapdump
        exclude: shutdown,env
    base-path: /actuator

swagger2:
  enable: true