package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.SettingJson;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FreemarkerOauthCreateCode extends WebCreateCode {

    public FreemarkerOauthCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
        isOauth = true;
        add("/freemarkerOauth/head.ftl", "head.ftl", "ftl", "common");
        add("/freemarkerOauth/left.ftl", "left.ftl", "ftl", "common");
        add("/freemarkerOauth/jspMain.ftl", "main.ftl", "ftl");
//        add("/freemarkerOauth/login.ftl", "login.ftl", "ftl");

//        add("/freemarker/jspList.ftl", "jspList.ftl", "templates");
        addData("controllerFtl", "/freemarkerOauth/controller.ftl");
//        addData("freemarkerFtl", "/freemarker/freemarkerConfig.ftl");
//        addData("jspListFtl", "jspList.ftl");
//        jsFtls.add("/bootstrap/jsList.ftl");

        if (data.getSettingJson().isShiro()) {
//            add("/freemarkerOauth/login.ftl", "login.ftl", "ftl");
        }
    }

    @Override
    protected void createOther(){
        super.createOther();

        if (table.isMiddle()) return;
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String configPath = createPath(projectRootPackage + ".configuration");
        new FreemarkerConfigMaker()
                .basePackage(projectRootPackage)
                .outPath(configPath)
                .author(data.getAuthorProperty())
                .ftl("/freemarkerOauth/freemarkerConfig.ftl")
                .make();

        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        String t = FreeMakerUtil.firstLower(table.getJavaName());
        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarkerOauth/jspList.ftl")
                .shiro(data.getSettingJson().isShiro())
                .fileName(t + "List.ftl")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarkerOauth/jspView.ftl")
                .fileName(t + "View.ftl")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarkerOauth/jspEdit.ftl")
                .fileName(t + "Edit.ftl")
                .make();


    }

    @Override
    protected void createOnce(){
        super.createOnce();

        String controllerPath = createPath(data.getControllerPackageProperty());
        new MainControllerMaker()
                .controllerPackage(data.getControllerPackageProperty())
                .author(data.getAuthorProperty())
                .ftl("freemarkerOauth/mainController.ftl")
                .fileName("MainController.java")
                .outPath(controllerPath)
                .make();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "resources"));
        try {
            SettingJson settingJson = data.getSettingJson();
            if (settingJson.isShiro()) {
                makerShiro();
            }
            copyResources(dirs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void makerShiro() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String configPath = createPath(projectRootPackage + ".configuration");

        new SimpleJavaMaker()
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/OAuth2ClientConfig.ftl")
                .fileName("OAuth2ClientConfig.java")
                .data("package", projectRootPackage)
                .projectRootPackage(projectRootPackage)
                .outPath(configPath)
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/WebResourceServerConfiguration.ftl")
                .fileName("WebResourceServerConfiguration.ftl")
                .outPath(configPath)
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/ClassPathTldsLoader.ftl")
                .fileName("ClassPathTldsLoader.java")
                .outPath(configPath)
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/ehcache.ftl")
                .fileName("ehcache.xml")
                .outPath(createResourcePath())
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/ehcache.ftl")
                .fileName("ehcache.xml")
                .outPath(createResourcePath("resources-prod"))
                .make();
    }

    private void copyResources(List<String> dirs) throws IOException {
        copyResources("tld", dirs);
        copyResources("common/js", dirs, "resource", "common", "js");
        copyResources("macro", dirs, "templates", "macro");
        copyResources("common/css", dirs, "resource", "common", "css");
        copyResources("common/bootstrap", dirs, "resource", "common");
        copyResources("frame/datetimepicker", dirs, "resource", "frame", "datetimepicker");
        copyResources("frame/validator", dirs, "resource", "frame", "validator");
    }

    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .ftl("/freemarkerOauth/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    protected void buildApplicationPropertiesMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarkerOauth/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-dev");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarkerOauth/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-prod");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarkerOauth/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

    }

    protected void buildApplicationYmlMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarkerOauth/application.ftl")
                .dbName(dbName)
                .fileName("application.yml")
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-dev");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarkerOauth/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-prod");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarkerOauth/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createTestResourcePath("resources");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .dBConnectionData(data)
                .ftl("applicationTest.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

    }

    protected String createJspPath(String... dirs) {
        String path = FileUtil.getSourceRoot();
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        for (String d : dirs) {
            dirst.add(d.toString());
        }
        path = makePath(path, dirst);
        return path;
    }

    protected String createJsPath() {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        path = makePath(path, dirs);
        return path;
    }

    protected String createJspPath() {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources", "templates", "ftl"));
        path = makePath(path, dirs);
        return path;
    }

    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
