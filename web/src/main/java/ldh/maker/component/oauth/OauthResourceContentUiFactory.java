package ldh.maker.component.oauth;

import ldh.maker.component.ContentUi;
import ldh.maker.component.ContentUiFactory;

public class OauthResourceContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new OauthResourceContentUi();
    }
}
