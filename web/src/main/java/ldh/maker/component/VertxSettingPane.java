package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class VertxSettingPane extends SettingPane {

    private VertxTableUi vertxTableUi;

    public VertxSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();
    }

    private void buildTableUiTab() {
        if (vertxTableUi == null) {
            vertxTableUi = new VertxTableUi(treeItem, dbName);
        }
        Tab tab = createTab("Table设置", vertxTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                vertxTableUi.show();
            }
        });
    }
}
