package ldh.maker.component;

import com.jfoenix.controls.JFXTabPane;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.util.Callback;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.JavafxClientCreateCode;
import ldh.maker.code.JavafxCreateCode;
import ldh.maker.database.TableInfo;
import ldh.maker.db.*;
import ldh.maker.freemaker.EnumStatusMaker;
import ldh.maker.util.*;
import ldh.maker.vo.JavafxSetting;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TableNo;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ldh on 2017/4/6.
 */
public class JavafxClientContentUi extends ContentUi {

    private VBox tableList;
    private TextField filterText;
    private ListView<Table> tableNamesList;
    private Button createAllCodeBtn = new Button("一键生成代码");

    private ObservableList<Table> tableValueList = FXCollections.observableArrayList();
    private FilteredList<Table> filteredData = new FilteredList<>(tableValueList, p -> true);

    private TabPane tabPane;
    private SplitPane splitPane;
    private TabPane contentPane;

    public JavafxClientContentUi() {
        createAllCodeBtn.getStyleClass().add("btn");
        createAllCodeBtn.getStyleClass().add("btn-primary");
    }

    public void setTreeItem(TreeItem<TreeNode> treeItem) {
        this.treeItem = treeItem;
        splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.HORIZONTAL);
        splitPane.setDividerPositions(0.4d);

        init();

        tabPane = new JFXTabPane();
        tabPane.setSide(Side.LEFT);
        Tab settingTab = new Tab("设置");
        settingTab.setClosable(false);
        TabPane settingPane = buildSettingPane(treeItem, treeItem.getValue().getData().toString());
        settingTab.setContent(settingPane);

        Tab coreTab = new Tab("主面板");
        coreTab.setClosable(false);
        coreTab.setContent(splitPane);

        tabPane.getTabs().addAll(settingTab, coreTab);
        tableList.setMaxWidth(200);
        this.setCenter(tabPane);

        if ((settingPane instanceof SettingPane) && ((SettingPane)settingPane).isSetting()) {
            tabPane.getSelectionModel().select(coreTab);
        }
    }

    private void init() {
        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        tableValueList.addAll(tableInfo.getTables().values());

        tableList = new VBox();
        filterText = new TextField("过滤");
        tableNamesList = new ListView<>();
        initListEvent();

        tableNamesList.setCellFactory(new Callback<ListView<Table>, ListCell<Table>>(){

            @Override
            public ListCell<Table> call(ListView<Table> param) {
                return new ListCell<Table>() {
                    @Override
                    public void updateItem(Table item, boolean empty) {
                        super.updateItem(item, empty);
                        Label label = new Label();
                        if (item != null) {
                            label.setText(item.getName());
                            if (item.getColumnList() != null && item.getPrimaryKey() == null) {
                                label.setStyle("-fx-background-color: red");
                            }
                            setGraphic(label);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });
        VBox.setVgrow(tableNamesList, Priority.ALWAYS);

        tableList.getChildren().addAll(filterText, tableNamesList, createAllCodeBtn);
        tableList.setSpacing(2d);
        tableList.setPadding(new Insets(5, 5, 2, 5));

        contentPane = new TabPane();
        splitPane.getItems().addAll(tableList, contentPane);

        createAllCodeBtn.setPrefWidth(200);
        tableNamesList.setItems(filteredData);
    }

    private void initListEvent() {
        filterText.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(table -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                System.out.println("newValue:" + newValue + ", tableName:" + table.getName() + ", size:" + filteredData.size());
                String lowerCaseFilter = newValue.toLowerCase();
                if (lowerCaseFilter.equals("过滤") || lowerCaseFilter.trim().equals("")) return true;
                if (table.getName().startsWith(lowerCaseFilter)) {
                    return true;
                }
                return false; // Does not match.
            });
        });

        filterText.setOnMouseEntered(e->{
            if (filterText.getText().equals("过滤")) {
                filterText.setText("");
            }
        });

        createAllCodeBtn.setOnAction(e->{
            saveAllData();
        });
    }

    private void reloadData() {
        tableValueList.clear();
        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        for (Table table : tableInfo.getTables().values()) {
            tableValueList.add(table);
        }
    }

    private Tab getTab(String selectTable) {
        for (Tab c : contentPane.getTabs()) {
            if (c.getText().equals(selectTable)) {
                return c;
            }
        }
        return null;
    }


    public TabPane getTabPane() {
        return contentPane;
    }

    public void saveAllData() {
        getTabPane().getTabs().clear();
        String dbName = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        try {
            SettingData data = SettingDb.loadData(treeItem.getParent().getValue(), dbName);
            if (data == null) {
                DialogUtil.show(Alert.AlertType.ERROR, "错误操作", "请设置项目的包路径");
                return;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            EnumDb.loadData(treeItem.getValue().getParent().getId(), dbName);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Task<Void> task = new Task<Void>() {
            @Override protected Void call() throws Exception {
                int i=0;
                int size = tableInfo.getTables().size();
                EnumDb.loadData(treeItem.getValue().getParent().getId(), dbName);
                updateMessage("开始生成代码");

                TableNo tableNo = TableNoDb.loadData(treeItem.getValue().getParent(), dbName);
                if (tableNo != null) {
                    List<String> tableNoes = tableNo.getTableNoes();
                    for (String tableName : tableNoes) {
                        if (tableInfo.getTable(tableName) != null)
                            tableInfo.getTable(tableName).setCreate(false);
                    }
                }

                i=0;
                for(Map.Entry<String, Table> entry : tableInfo.getTables().entrySet()) {
                    i++;
                    try {
                        Table table = entry.getValue();
                        if (table.getPrimaryKey() == null || !table.isCreate()) {
                            updateMessage("开始生成第" + i + "个代码， 总" + size);
                            updateProgress(i, tableInfo.getTables().size());
                            continue;
                        }
                        SettingData data = SettingDb.loadData(treeItem.getValue().getParent(), dbName);
                        CreateCode createCode = buildCreateCode(data, treeItem, dbName, table);
                        updateMessage("开始生成第" + i + "个代码， 总" + size);
                        updateProgress(i, tableInfo.getTables().size());
                        createCode.create();
                        Thread.sleep(50);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                updateProgress(tableInfo.getTables().size(), tableInfo.getTables().size());
                done();
                return null;
            }
        };

        UiUtil.STATUSBAR.textProperty().bind(task.messageProperty());
        UiUtil.STATUSBAR.progressProperty().bind(task.progressProperty());

        // remove bindings again
        task.setOnSucceeded(event -> {
            UiUtil.STATUSBAR.textProperty().unbind();
            UiUtil.STATUSBAR.progressProperty().unbind();
            Platform.runLater(()->{
                DialogUtil.show(Alert.AlertType.INFORMATION, "代码生成完成", "代码生成完成");
            });
        });

        new Thread(task).start();

    }

    protected  CreateCode buildCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        return new JavafxClientCreateCode(data, treeItem, dbName, table);
    }

    protected TabPane buildSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        return new JavafxClientSettingPane(treeItem, dbName);
    }
}
