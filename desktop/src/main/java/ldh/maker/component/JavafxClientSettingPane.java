package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class JavafxClientSettingPane extends SettingPane {

    private JavafxClientTableUi javafxClientTableUi;

    public JavafxClientSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();
    }

    private void buildTableUiTab() {
        javafxClientTableUi = new JavafxClientTableUi(treeItem, dbName);
        Tab tab = createTab("Table设置", javafxClientTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                javafxClientTableUi.show();
            }
        });
    }

    @Override
    protected PackagePane buildPackagePaneTab() {
        JavafxPackagePane packagePane = new JavafxPackagePane(treeItem, dbName);
        createTab("路径设置", packagePane);
        return packagePane;
    }

    @Override
    protected void buildAliasPaneTab() {

    }
}
