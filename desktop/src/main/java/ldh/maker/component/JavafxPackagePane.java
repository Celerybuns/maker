package ldh.maker.component;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import ldh.maker.db.SettingDb;
import ldh.maker.util.ConnectionFactory;
import ldh.maker.util.DialogUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.SettingJson;
import ldh.maker.vo.TreeNode;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by ldh123 on 2018/6/9.
 */
public class JavafxPackagePane extends PackagePane {

    ValidationSupport validationSupport = new ValidationSupport();

    private SettingData settingData;

    private TextField javafxPojoField;

    public JavafxPackagePane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

    @Override
    protected void initUi() {
        validationSupport = new ValidationSupport();
        GridPane gridPane = new GridPane();
        gridPane.setVgap(20);
        gridPane.setHgap(20);
        gridPane.setPadding(new Insets(20, 20, 20,20));
        Label label = new Label("设置Javafx的包路径");
        GridPane.setConstraints(label, 0, 0, 2, 1);

        Label packageLabel = new Label("包路径");
        javafxPojoField = new TextField();
        GridPane.setConstraints(packageLabel, 0, 1);
        GridPane.setConstraints(javafxPojoField, 1, 1);

        Button button = new Button("提交");
        button.setOnAction(e->saveBtn());
        GridPane.setConstraints(button, 0, 2, 2, 1);

        validationSupport.registerValidator(javafxPojoField, Validator.createRegexValidator("Javafx包路径不能为空", "^([a-zA-Z]+[.]{0,1})+$", Severity.ERROR));
        gridPane.getChildren().addAll(label, packageLabel, javafxPojoField, button);
        this.getChildren().add(gridPane);

        loadData(treeItem);
    }

    private void loadData(TreeItem<TreeNode> treeItem) {
        try {
            settingData = SettingDb.loadData(treeItem.getParent().getValue(), dbName);
            if (settingData == null || settingData.getSettingJson().getJavafxPojoPath() == null)return;
            javafxPojoField.setText(settingData.getSettingJson().getJavafxPojoPath());
            isSetting = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveBtn() {
        if(validationSupport.isInvalid()) {
            DialogUtil.show(Alert.AlertType.WARNING, "填写错误", "请按照要求填写");
            return;
        }
        if (settingData == null) {
            settingData = new SettingData();
            settingData.setDbName(dbName);
        }
        SettingJson settingJson = settingData.getSettingJson();
        if (settingJson == null) settingJson = new SettingJson();
        settingJson.setJavafxPojoPath(javafxPojoField.getText().trim());
        settingData.setSettingJson(settingJson);
        DBConnectionData data = (DBConnectionData) treeItem.getValue().getParent().getData();
        Connection connection = data.getConnection();
        if (connection == null) {
            connection = ConnectionFactory.getConnection(data);
            data.setConnection(connection);
        }
        if (connection == null) {
            DialogUtil.show(Alert.AlertType.WARNING, "填写错误", "数据库连接不上");
            return;
        }

        try {
            SettingData data2 = SettingDb.loadData(treeItem.getParent().getValue(), dbName);
            if (data2 == null) {
                SettingDb.save(settingData, treeItem.getParent().getValue());
            } else {
                data2.setSettingJson(settingJson);
                SettingDb.update(data2, treeItem.getParent().getValue());
            }
            DialogUtil.show(Alert.AlertType.INFORMATION, "成功", "设置成功");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
